# -ITM- Chevereto Integration 2.0

A simple add-on that allows Xenforo admins to integrate their Chevereto image hosting.

### Installation

1. Auto-installation
   1. Go to ACP->Add-Ons and upload the ZIP file

2. Manually installation
   1. Upload all files in `/upload` folder in your forum root
   2. Got to ACP->Add-Ons an install the new listed add-on

### Description

The popup upload plugin (PUP) is a small file that allows Chevereto based websites to provide external image uploading via a small JavaScript file.

### Requirements

- PHP 7.1.0+

### Options
![Options](https://bitbucket.org/McAtze/itm-cheveretointegration/raw/17149dd12dc694491b404d1cb9706a1e6755ef9d/screenshots/options.png)

### Permissions
![Permissions](https://bitbucket.org/McAtze/itm-cheveretointegration/raw/17149dd12dc694491b404d1cb9706a1e6755ef9d/screenshots/Permissions.png)

### Screenshots
#### XenForo 2.2
![In Editor row](https://bitbucket.org/McAtze/itm-cheveretointegration/raw/17149dd12dc694491b404d1cb9706a1e6755ef9d/screenshots/XF22-inEditorRow.png)
![Before Attachment](https://bitbucket.org/McAtze/itm-cheveretointegration/raw/17149dd12dc694491b404d1cb9706a1e6755ef9d/screenshots/XF22-beforeAttachments.png)
![After Attachment](https://bitbucket.org/McAtze/itm-cheveretointegration/raw/17149dd12dc694491b404d1cb9706a1e6755ef9d/screenshots/XF22-afterAttachments.png)

#### XenForo 2.1
![Thread-After](https://bitbucket.org/McAtze/itm-cheveretointegration/raw/17149dd12dc694491b404d1cb9706a1e6755ef9d/screenshots/Thread-After.png)
![Thread-Before](https://bitbucket.org/McAtze/itm-cheveretointegration/raw/17149dd12dc694491b404d1cb9706a1e6755ef9d/screenshots/Thread-Before.png)
![Thread-Button-Blue](https://bitbucket.org/McAtze/itm-cheveretointegration/raw/17149dd12dc694491b404d1cb9706a1e6755ef9d/screenshots/Thread-Button-Blue.png)
![Thread-Button-DarkBlue](https://bitbucket.org/McAtze/itm-cheveretointegration/raw/17149dd12dc694491b404d1cb9706a1e6755ef9d/screenshots/Thread-Button-Darkblue.png)
![Thread-Button-Orange](https://bitbucket.org/McAtze/itm-cheveretointegration/raw/17149dd12dc694491b404d1cb9706a1e6755ef9d/screenshots/Thread-Button-Orange.png)
![Thread-Editor](https://bitbucket.org/McAtze/itm-cheveretointegration/raw/17149dd12dc694491b404d1cb9706a1e6755ef9d/screenshots/Thread-Button-Editor.png)
![Conversation-Button](https://bitbucket.org/McAtze/itm-cheveretointegration/raw/17149dd12dc694491b404d1cb9706a1e6755ef9d/screenshots/Conversation-Button.png)
![Thread-Embed-BBCode](https://bitbucket.org/McAtze/itm-cheveretointegration/raw/17149dd12dc694491b404d1cb9706a1e6755ef9d/screenshots/Thread-Embed-BBCode.png)

### After words

If you wanna thank me for this add-on just [PayPal.Me](https://www.paypal.me/itmaku) ..
